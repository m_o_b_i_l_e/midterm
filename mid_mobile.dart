import 'dart:io';
import 'dart:math';

List<String> listInfix = <String>[];
List<String> listPostfix = <String>[];
List<String> listOperators = <String>[];
List<int> listValues = <int>[];

List tokenizing(String str) {
  List<String> listTokenizing = str.split(" ");
  return listTokenizing;
}

List<String> InfixToPostfix(List liststr) {

  for (int i = 0; i < liststr.length; i++) {
    
    if (double.tryParse(liststr[i]) != null) {
      listPostfix.add((liststr[i]));
    }
    if (liststr[i] == "+" ||
        liststr[i] == "-" ||
        liststr[i] == "*" ||
        liststr[i] == "/" ||
        liststr[i] == "^") {
      while (listOperators.isNotEmpty &&
          listOperators.last != "(" &&
          (liststr[i] == "+" ||
              liststr[i] == "-" && liststr.last == "*" ||
              listOperators.last == "/" ||
              listOperators.last == "^")) {
        listPostfix.add(listOperators.removeLast());
      }
  }
      while (listOperators.isNotEmpty &&
          listOperators.last != "(" && (liststr[i] == "*" || liststr[i] == "/" && listOperators.last == "*" || listOperators.last == "/" ||
              listOperators.last == "^")) {
        listPostfix.add(listOperators.removeLast());
      }
      listOperators.add(liststr[i]);
    }

  while (listOperators.isNotEmpty) {
    listPostfix.add(listOperators.removeLast());
  }
  return listPostfix;
}

void main(List<String> arguments) {
  //input 
  print("Input String : ");
  String? str = stdin.readLineSync()!;
  print("Tokenizing a String : ");
  print(tokenizing(str));

  print("Infix to Postfix : ");
  InfixToPostfix(listInfix);
  print(listInfix);

  print("Evaluate Postfix : ");
  print("");
}